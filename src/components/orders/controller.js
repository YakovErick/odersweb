import { mapGetters } from 'vuex';
import OrderForm from './create';
import * as service from '../../services/OrderDetails';
import { Message } from 'element-ui';

const OrderController = {
    data() {
        return {
            selected_items: [],
            delivery_amount: 0,
            mark_delivery: false,
            form: {},
            editForm: {},
            editModal: false,
            detailUpdating: false
        }
    },

    components: {
        OrderForm
    },

    computed: {
        ...mapGetters({
            order: 'order',
            orderModal: 'orderModal',
            savingDelivery: 'savingDelivery',
            allProducts: 'allProducts'
        })
    },

    methods: {

        fetchOrderDetails() {
            this.$store.commit('FETCH_ORDER_DETAILS', this.$route.params.id);
        },

        submit() {
            this.$store.commit('MARK_AS_DELIVERED', {
                order_id: this.$route.params.id,
                items: this.selected_items
            });
        },

        markAsDelivered() {

            this.$confirm('This will mark item(s) as delivered. Continue?', 'Warning', {
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
                type: 'warning'
            }).then(() => {

                this.submit();

            }).catch(() => {
                this.$message({
                    type: 'info',
                    message: 'Delivered canceled'
                });
            });
        },

        deliver(row) {
            this.form = row;
            this.mark_delivery = true;
        },

        addDelivery() {
            this.selected_items.push(this.form);
            this.submit();

            this.form = {};
            this.selected_items = [];

            this.mark_delivery = false;
        },

        removeItem(id) {
            this.$confirm('This will remove the item from the list. Continue?', 'Warning', {
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
                type: 'warning'
            }).then(() => {

                service.deleteOrderDetail(id)
                    .then( () => {
                        Message.success({ message: 'Item deleted successfully!'});
                        this.fetchOrderDetails();
                    })

            }).catch(() => {
                this.$message({
                    type: 'info',
                    message: 'Deletion canceled'
                });
            });
        },

        addItem() {
            this.$store.commit('ORDER_MODAL', true);
        },

        editItem(row) {
            this.editForm = row;
            this.editModal = true;
        },

        submitEdit() {

            this.detailUpdating = true;

            service.updateOrderDetail(this.editForm)
                .then( () => {
                    this.detailUpdating = false;

                    Message.success({ message: 'Item updated successfully!'});

                    this.editModal = false;

                    this.fetchOrderDetails();
                }, () => {
                    this.detailUpdating = false;
                });
        },

        toggleSelection(rows) {
            if (rows) {
                rows.forEach(row => {
                    this.$refs.multipleTable.toggleRowSelection(row);
                });
            } else {
                this.$refs.multipleTable.clearSelection();
            }
        },

        handleSelectionChange(val) {
            this.selected_items = val;
        },

        tableRowClassName(rowIndex) {
            if (rowIndex === 1) {
                return 'warning-row';
            } else if (rowIndex === 3) {
                return 'success-row';
            }
            return '';
        }
    },

    mounted() {
        this.fetchOrderDetails();

        this.$store.commit('ALL_PRODUCTS');
    }
};

export default OrderController;
