import Vue from 'vue';
import moment from 'moment';

const currencyFilter = function (value, currency, decimals) {

    value = parseFloat(value);

    if (!isFinite(value) || (!value && value !== 0)) {
        return '';
    }

    decimals = decimals != null ? decimals : 2;

    currency = currency != null ? currency : '';

    var digitsRE = /(\d{3})(?=\d)/g;

    var stringified = Math.abs(value).toFixed(decimals);

    var _int = decimals
        ? stringified.slice(0, -1 - decimals)
        : stringified;

    var i = _int.length % 3;

    var head = i > 0
        ? (_int.slice(0, i) + (_int.length > 3 ? ',' : ''))
        : '';

    var _float = decimals
        ? stringified.slice(-1 - decimals)
        : '';

    var sign = value < 0 ? '-' : '';

    return sign + currency + head +
        _int.slice(i).replace(digitsRE, '$1,') +
        _float;
};


const amountFilter = function (val) {
    let use = Math.abs(val);

    let out = currencyFilter(use);

    if (val >= 0) {
        return out;
    }

    return out;
};

const dateFilter = function (date) {
    return moment(date).format("MMM DD, YYYY");
};


Vue.filter('currency', currencyFilter);

Vue.filter('amount', amountFilter);

Vue.filter('date', dateFilter);

export default {
    currencyFilter,
    dateFilter,
    amountFilter
};
