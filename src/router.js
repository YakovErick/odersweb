import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import OrderIndex from './components/orders/index';
import OrderShow from './components/orders/show';

import SuppliersIndex from './components/suppliers/index';
import SuppliersShow from './components/suppliers/show';

import ProductsIndex from './components/products/index';
import ProductsShow from './components/products/show';

import DeliveryIndex from './components/deliveries/index';

const router = new VueRouter({
    mode: 'history',

    routes : [
        {
            name: 'home.index',
            path: '/',
            component: OrderIndex,
        },

        {
            name: 'suppliers.index',
            path: '/suppliers',
            component: SuppliersIndex,
        },
        {
            name: 'suppliers.show',
            path: '/suppliers/:id',
            component: SuppliersShow,
        },

        {
            name: 'products.index',
            path: '/products',
            component: ProductsIndex,
        },

        {
            name: 'products.show',
            path: '/products/:id',
            component: ProductsShow,
        },

        {
            name: 'orders.index',
            path: '/orders',
            component: OrderIndex,
        },
        {
            name: 'orders.show',
            path: '/orders/:id',
            component: OrderShow,
        },

        {
            name: 'deliveries.index',
            path: '/deliveries',
            component: DeliveryIndex,
        }
    ]
});

export default router;

router.beforeEach((to, from, next) => {
    next();
});
