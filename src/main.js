import Vue from 'vue';
import App from './App.vue';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

require('./filters');

/**
 * Set up element ui
 */
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/en'

Vue.use(ElementUI, { locale });

Vue.config.productionTip = false;

import router from './router';
import store from './store/index'

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app');
