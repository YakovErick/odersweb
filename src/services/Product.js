import axios from 'axios';
import * as config from '../config';

let baseUrl = config.baseUrl;

export const fetchProducts = function () {
    return axios.get(baseUrl + '/products');
};

export const fetchProductDetails = function (id) {
    return axios.get(baseUrl + '/products/'+id);
};

export const submitProduct = function (form) {
    return axios.post(baseUrl + '/products', form);
};

export const deleteProduct = function (id) {
    return axios.delete(baseUrl+'/products/'+id);
};

export const fetchAllProducts = function () {
    return axios.get(baseUrl+'/all-products');
};
