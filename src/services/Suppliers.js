import axios from 'axios';
import * as config from '../config';

let baseUrl = config.baseUrl+'/suppliers';

export const fetchSuppliers = function () {
    return axios.get(baseUrl);
};

export const fetchSupplierDetails = function (id) {
    return axios.get(baseUrl + '/'+id);
};

export const submitSupplier = function (form) {
    return axios.post(baseUrl, form);
};

export const deleteSupplier = function (id) {
    return axios.delete(baseUrl+'/'+id);
};
