import axios from 'axios';
import * as config from '../config';

let baseUrl = config.baseUrl;

export const fetchDeliveries = function () {
    return axios.get(baseUrl + '/deliveries');
};

