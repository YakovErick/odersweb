import axios from 'axios';
import * as config from '../config';

let baseUrl = config.baseUrl+'/orders';

export const fetchOrders = function () {
    return axios.get(baseUrl);
};

export const fetchOrderDetails = function (id) {
    return axios.get(baseUrl + '/'+id);
};

export const submitOrder = function (form) {
    return axios.post(baseUrl, form);
};

export const deleteOrder = function (id) {
    return axios.delete(baseUrl+'/'+id);
};
