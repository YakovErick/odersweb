import axios from 'axios';
import * as config from '../config';

let baseUrl = config.baseUrl;

export const updateOrderDetail = function (form) {
    return axios.put(baseUrl+'/'+form.id, form);
};

export const deleteOrderDetail = function (id) {
    return axios.delete(baseUrl+'/'+id);
};

export const markAsDelivered = function (form) {
    return axios.post(baseUrl+'/delivery/save', form);
};
