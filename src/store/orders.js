import * as service from '../services/Orders';
import {Message} from "element-ui";
import store from '../store';

const state = {

    orders: [],
    fetchingOrders: false,

    order: {},
    orderModal: false,

    submittingOrder: false
};

const getters = {

    orders: state => state.orders,
    fetchingOrders: state => state.fetchingOrders,

    order: state => state.order,
    orderModal: state => state.orderModal,

    submittingOrder: state => state.submittingOrder
};

const mutations = {

    FETCH_ORDERS(state) {

        state.fetchingOrders  = true;

        service.fetchOrders()
            .then( ({ data }) => {

                state.orders = data.data;
                state.fetchingOrders  = false;

            }, () => {
                state.fetchingOrders  = false;
            });
    },

    FETCH_ORDER_DETAILS(state, id) {

        state.fetchingOrders  = true;

        service.fetchOrderDetails(id)
            .then( ({ data }) => {

                state.order = data;
                state.fetchingOrders  = false;

            }, () => {
                state.fetchingOrders  = false;
            });
    },

    SUBMIT_ORDER(state, form) {

        state.submittingOrder  = true;

        service.submitOrder(form)
            .then( () => {

                Message.success({ message: 'Order added successfully!'});
                state.submittingOrder  = false;

                state.orderModal  = false;

                form.order_id

                    ? store.commit('FETCH_ORDER_DETAILS', form.order_id)

                    : store.commit('FETCH_ORDERS', form.order_id);

            }, () => {
                state.submittingOrder  = false;
            });
    },

    ORDER_MODAL(state, value) {
        state.orderModal = value;
    }
};

export default {
    state, getters, mutations
};
