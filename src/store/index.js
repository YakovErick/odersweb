import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import Suppliers from './suppliers';
import Products from './products';
import Orders from './orders';
import OrderDetails from './orderDetails';
import Dlieveries from './deliveries';

const store = new Vuex.Store({

    modules: {
        Suppliers,
        Products,
        Orders,
        OrderDetails,
        Dlieveries
    }
});

export default store;
