import * as service from '../services/OrderDetails';
import store from '../store/index';

const state = {
    savingDelivery: false
};

const getters = {

    savingDelivery: state => state.savingDelivery
};

const mutations = {

    MARK_AS_DELIVERED(state, form) {

        state.savingDelivery = true;

        service.markAsDelivered(form)
            .then( () => {
                state.savingDelivery = true;
                store.commit('FETCH_ORDER_DETAILS', form.order_id);

                state.savingDelivery = false;
            }, () => {
                state.savingDelivery = false;
            });
    }
};

export default {
    state, getters, mutations
};
