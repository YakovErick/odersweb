import * as service from '../services/Suppliers';
import { Message } from 'element-ui';
import store from '../store';

const state = {
    suppliers: [],

    fetchingSuppliers: false,
    submittingSupplier: false,

    supplier: {},
    selected_supplier: {},

    supplierModal: false,
};

const getters = {

    suppliers: state => state.suppliers,

    fetchingSuppliers: state => state.fetchingSuppliers,
    submittingSupplier: state => state.submittingSupplier,

    supplier: state => state.supplier,
    selected_supplier: state => state.selected_supplier,

    supplierModal: state => state.supplierModal,
};

const mutations = {

    FETCH_SUPPLIERS(state) {

        state.fetchingSuppliers  = true;

        service.fetchSuppliers()
            .then( ({ data }) => {

                state.suppliers = data.data;
                state.fetchingSuppliers  = false;

            }, () => {
                state.fetchingSuppliers  = false;
            });
    },

    SUBMIT_SUPPLIER(state, form) {

        state.submittingSupplier  = true;

        service.submitSupplier(form)
            .then( () => {

                Message.success({ message: 'Supplier added successfully!'});
                state.submittingSupplier  = false;

                store.commit('FETCH_SUPPLIERS');

                state.supplierModal  = false;

            }, () => {
                state.submittingSupplier  = false;
            });
    },

    FETCH_SUPPLIER_DETAILS(state, id) {

        state.fetchingSuppliers  = true;

        service.fetchSupplierDetails(id)
            .then( ({ data }) => {

                state.supplier = data;
                state.fetchingSuppliers  = false;

            }, () => {
                state.fetchingSuppliers  = false;
            });
    },

    SET_SELECTED_SUPPLIER(state, id) {

        if(id) {
            state.selected_supplier = state.suppliers.filter( supplier => supplier.id == id)[0];
        } else {
            state.selected_supplier = {};
        }
    },

    SUPPLIER_MODAL(state, value) {
        state.supplierModal = value;
    }
};

export default {
    state, getters, mutations
};
