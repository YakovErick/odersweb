import * as service from '../services/Product';
import { Message } from 'element-ui';
import store from '../store/index';

const state = {
    products: [],

    fetchingProducts: false,
    submittingProduct: false,

    product: {},
    selected_product: {},

    productModal: false,

    allProducts: []
};

const getters = {

    products: state => state.products,

    fetchingProducts: state => state.fetchingProducts,
    submittingProduct: state => state.submittingProduct,

    product: state => state.product,
    selected_product: state => state.selected_product,

    productModal: state => state.productModal,

    allProducts: state => state.allProducts
};

const mutations = {

    FETCH_PRODUCTS(state) {

        state.fetchingProducts  = true;

        service.fetchProducts()
            .then( ({ data }) => {

                state.products = data.data;
                state.fetchingProducts  = false;

            }, () => {
                state.fetchingProducts  = false;
            });
    },

    SUBMIT_PRODUCT(state, form) {

        state.submittingProduct  = true;

        service.submitProduct(form)
            .then( () => {

                Message.success({ message: 'Product added successfully!'});
                state.submittingProduct  = false;

                store.commit('FETCH_SUPPLIER_DETAILS', form.supplier_id);

                state.productModal  = false;

            }, () => {
                state.submittingProduct  = false;
            });
    },

    FETCH_PRODUCT_DETAILS(state, id) {

        state.fetchingProducts  = true;

        service.fetchProductDetails(id)
            .then( ({ data }) => {

                state.product = data;
                state.fetchingProducts  = false;

            }, () => {
                state.fetchingProducts  = false;
            });
    },

    SET_SELECTED_PRODUCT(state, id) {
        if(id) {
            state.selected_product = state.products.filter( product => product.id === id)[0];
        } else {
            state.selected_product = {};
        }
    },

    PRODUCT_MODAL(state, value) {
        state.productModal = value;
    },

    ALL_PRODUCTS(state) {
        service.fetchAllProducts()
            .then( ({ data }) => {
                state.allProducts = data;
            });
    }
};

export default {
    state, getters, mutations
};
