import * as service from '../services/Deliveries';

const state = {
    deliveries: [],
    fetchingDeliveries: false
};

const getters = {
    deliveries: state => state.deliveries,
    fetchingDeliveries: state => state.fetchingDeliveries,
};

const mutations = {

    FETCH_DELIVERIES(state) {

        state.fetchingDeliveries = true;

        service.fetchDeliveries()
            .then( ({ data }) => {

                state.fetchingDeliveries = false;
                state.deliveries = data.data;

            }, () => {
                state.fetchingDeliveries = false;
            });
    }
};

export default {
    state, getters, mutations
};
